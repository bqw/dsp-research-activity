# coding: utf-8

"""
Описание базисных функций и базовых алгоритмов для формирования матриц ядра преобразований
различных спектров
"""

import collections
import itertools
import math

import numpy as np


class BasicFunctions(object):
    """
    Базисные функции
    """

    # Порядок матрицы
    N = 32

    @classmethod
    def sin_f(cls, k, i):
        """
        Тригонометрический базис
        """

        res = math.sin(2 * math.pi * k * i / cls.N)

        if abs(res) > 10 ** (-10):
            return res
        else:
            return 0.0

    @classmethod
    def cos_f(cls, k, i):
        """
        Тригонометрический базис
        """

        res = math.cos(2 * math.pi * k * i / cls.N)

        if abs(res) > 10 ** (-10):
            return res
        else:
            return 0.0

    @classmethod
    def cas_f(cls, k, i):
        """
        Базис Хартли
        """

        res = cls.sin_f(k, i) + cls.cos_f(k, i)

        if abs(res) > 10 ** (-10):
            return res
        else:
            return 0.0

    @classmethod
    def wal_f(cls, k, i):
        """
        Базис Уолша
        """

        bin_k, bin_i = '{0:b}'.format(abs(k)), '{0:b}'.format(abs(i))

        bin_k = [int(x) for x in '0' * (cls.N - len(bin_k)) + bin_k]
        bin_i = [int(x) for x in '0' * (cls.N - len(bin_i)) + bin_i]

        return (-1) ** sum([k * i for k, i in zip(bin_k, bin_i)])


"""
Построение матриц значений базисных функций
"""


def build_tr_matrix(rank, r=None):
    """
    Построение матрицы значений базисных функций тригонометрического базиса
    заданного порядка (вынесено в отдельную функцию, т.к. требует работы с четными
    и нечетными составляющими)

    Вид матрицы: по строкам базисные функции, по столбцам отсчеты (i)

    :param rank: Порядок матрицы
    :param r: ДИапазон изменения индекса i (отсчет базисной функции)
    :return list[list[float]]: Полученная матрица

    """

    matrix = list()

    # Формирование диапазонов индексов для нечетных и четных функции по отдельности,
    # затем слияние их в один массив
    even_range = range(rank // 2 + 1)
    odd_range = range(1, rank // 2)

    m_range = list()

    for e_item, o_item in itertools.zip_longest(even_range, odd_range):
        if e_item is not None:
            m_range.append(e_item)

        if o_item is not None:
            m_range.append(o_item)

    # Порядок следования четных фунций
    even_functions = [1, 0] * (rank // 2)
    even_functions[-1] = 1

    if r is None:
        r = range(rank)

    # Вычисление матрицы значений функции

    for m, even in zip(m_range, even_functions):
        row = list()

        for i in r:
            if even:
                row.append(BasicFunctions.cos_f(m, i))
            else:
                row.append(BasicFunctions.sin_f(m, i))

        matrix.append(row)

    return matrix

def build_matrix(rank, f, r=None):
    """
    Построение матрицы значений базисных функций Уолша-Адамара
    заданного порядка

    Вид матрицы: по строкам базисные функции, по столбцам отсчеты (i)

    :param rank: Порядок матрицы
    :param f: Базисная функция
    :param r: Диапазон изменения индекса i (номера отсчета базисной функции)
    :return list[list[float]]: Полученная матрица

    """

    if r is None:
        r = range(rank)

    matrix = list()

    for k in range(rank):
        row = list()

        for i in r:
            row.append(f(k, i))

        matrix.append(row)

    return matrix
